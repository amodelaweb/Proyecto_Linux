# AGENDA CON LISTA NEGRA Y MANEJADOR IMG

Se encuentra una agenda telefónica con lista negra y un manejador que permite copiar sus fotos y vídeos a un directorio especifico creando también sus respectivos Thumbnails

## Installation

### Requirements
* Linux
* Python 3.5 and up

`$ pip3 install -r requirements.txt `

## Usage

`$ python3 agenda.py `

`$ python3 fotos.py -s ~/Pictures/FOTOS/ -d '/home/amodelaweb/Pictures/Mis\ Fotos/' `
*  El '-s' es para especificar la fuente, trata de simular "source "
*  El '-d' es para especificar el destino de las imagenes "Destination"
* Es importante tener en cuenta que las banderas -s y -d pueden estat en cualquier orden en tanto que se cumpla de propiedad de que el path debe estar justo despues de la bandera 


## License
[GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) ~ Santiago Chaustre Perdomo
