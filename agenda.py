import os
import csv
import ast

class Persona:
    ## CONSTRUCTOR
    def __init__ (self , number , name , direction = "-" , state = True):
        self.__number = []
        self.__number.append(number)
        self.__name = name
        self.__state = state
        self.__direction = direction
    # GETTER Y SETTERS
    # ATRIBUTO NUMERO
    @property
    def number(self):
        return self.__number
    @number.setter
    def number(self,number):
        self.__number.append(number)
    @number.setter
    def number2(self , number):
        self.__number = number
    # ATRIBUTO NOMBRE
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self,name):
        self.__name = name
    # ATRIBUTO ESTADO
    @property
    def state(self):
        return self.__state
    @state.setter
    def state(self,state):
        self.__state = state
    # ATRIBUTO DIRECCION
    @property
    def direction(self):
        return self.__direction
    @direction.setter
    def direction(self,direction):
        self.__direction = direction
    ## IMPRESION DE DATOS
    def __repr__(self):
        res = ""
        for x in self.__number :
            res += ("| {:^20} | {:^15} | {:^15} | {:^15} |".format(self.__name[:20],x,"No registra !" if self.__direction == "-" else self.__direction[:15], "Me Cae Bien" if self.__state == True else "Me Cae Mal"))
            res += "\n"
        return res
class Agenda:
    def __init__(self,nombre = ""):
        self.__contactos = []
        self.__nombre = nombre
    ## GETTERS AND SETTERS
    # ATRIBUTO CONTACTOS
    @property
    def contactos (self):
        return self.__contactos
    @contactos.setter
    def contactos (self, persona):
        ban = False
        for x in self.__contactos:
            if x.name.lower() == persona.name.lower():
                x.number = persona.number[0]
                ban = True
        if ban == False:
            self.__contactos.append(persona)
    # ATRIBUTO NOMBRE
    @property
    def nombre (self):
        return self.__nombre
    @nombre.setter
    def nombre (self, nombre ):
        self.__nombre = nombre
    ## METODOS DE LISTAR ESPECIFICAMENTE
    def is_vacio (self):
        if size(self.__contactos) == 0:
            return True
        return False
    def cabecera (self):
        ret = ""
        ret += ("+ {:^20} - {:^15} - {:^15} - {:^15} +".format("--------------------","---------------","---------------", "---------------"))
        ret +=  "\n"
        ret += ("| {:^20} | {:^15} | {:^15} | {:^15} |".format("NOMBRE","NUMERO","DIRECCION","ESTADO"))
        ret +=  "\n"
        ret += ("| {:^20} + {:^15} + {:^15} | {:^15} |".format("--------------------","---------------","---------------", "---------------"))
        ret +=  "\n"
        return ret
    def footer(self):
        return ("+ {:^20} - {:^15} - {:^15} - {:^15} +".format("--------------------","---------------","---------------", "---------------"))
    @property
    def me_cae_mal (self):
        ret = self.cabecera()
        ban = False
        if len(self.__contactos ) > 0 :
            for x in self.__contactos:
                if x.state == False:
                    ret += str(x)
                    ban = True
            ret += self.footer()
        if ban ==  False:
            width = 78
            ret = "\n"
            ret += (" Lista vacia !! \n").center(width)
        return ret
    @property
    def me_cae_bien (self):
        ret = self.cabecera()
        ban = False
        if len(self.__contactos ) > 0 :
            for x in self.__contactos:
                if x.state == True:
                    ret += str(x)
                    ban = True
            ret += self.footer()
        if ban ==  False:
            width = 78
            ret = "\n"
            ret += (" Lista vacia !! \n").center(width)
        return ret
    @property
    def list_all(self):
        width = 78
        ret = "\n"
        ret += (" Lista vacia !! \n").center(width)
        if len(self.__contactos ) > 0 :
            ret = self.cabecera()
            for x in self.__contactos:
                ret += str(x)
            ret += self.footer()
        return ret
    ## METODOS UTILIITARIOS
    def borrar_contacto (self , name):
        width = 78
        ban = False
        str = "\n"
        str += (" No se encontro el contacto a borrar ! \n").center(width)
        for i in range(0,len(self.__contactos)):
            if ((self.__contactos[i]).name).lower() == name.lower() :
                ban = True
                index = i
                break
        if ban :
            str = "\n"
            str += (" Se ha borrado  exitosamente ! \n").center(width)
            del self.__contactos[index]
        return str
    def get_info_of (self,name):
        ret = self.cabecera()
        ban = False
        for i in range(0,len(self.__contactos)):
            if ((self.__contactos[i]).name).lower() == name.lower() :
                ban = True
                ret += str(self.__contactos[i])
                break
        ret += self.footer()
        if ban == False :
            width = 78
            ret = "\n"
            ret += (" Numero de %s no encontrado !  \n"%name).center(width) ;

        return ret
def guardar_csv(path , agenda):
    width = 78
    try:
        with open(path, 'w') as csvfile:
            fieldnames = ['Nombre', 'Numero' , 'Direccion' , 'Estado']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            for x in agenda.contactos:
                writer.writerow({'Nombre': x.name, 'Numero': x.number , 'Direccion': x.direction , 'Estado': x.state})
        print("\n")
        print ("Guardado exitoso ! \n\n".center(width))
    except IOError:
        width = 78
        print("\n")
        print ("El fichero que guarda no se guardo ! \n".center(width))
        print ("Presione una tecla para continuar.....\n\n".center(width))
        input()
def cargar_csv(path,name=""):
    y = Agenda(name)
    try:
        with open(path, 'r') as csvfile:
             reader = csv.DictReader(csvfile)
             for row in reader:

                 if row['Estado'] == "True":
                     estado = True
                 else:
                     estado = False
                 a = Persona ("1",row['Nombre'],row['Direccion'],estado)
                 a.number2 = ast.literal_eval(row['Numero'])
                 y.contactos = a
    except IOError:
        width = 78
        print("\n")
        print ("El fichero que busca no existe ! \n".center(width))
        print ("Presione una tecla para continuar.....\n\n".center(width))
        input()
    return y
def Print_Menu(name):
    width = 78
    print(("-- AGENDA DE %s -- \n"%(name.upper())).center(width))
    print(("1. ) Agregar nuevo contacto. \n").center(width))
    print(("2. ) Eliminar contacto. \n").center(width))
    print(("3. ) Listar contactos. \n").center(width))
    print(("4. ) Buscar Informacion de contacto. \n").center(width))
    print(("5. ) Guardar contactos en csv. \n").center(width))
    print(("6. ) Cargar contactos desde csv. \n").center(width))
    print(("7. ) Salir. \n\n").center(width))
    var = input("\t\t\t Eleccion : " )
    return var
def Print_Menu_List():
    width = 78
    print("\n")
    print(("--- MENU DE LISTADO ---\n").center(width))
    print(("1. ) Listar todos los contactos. \n").center(width))
    print(("2. ) Listar contactos que me caen bien. \n").center(width))
    print(("3. ) Listar contactos que me caen mal. \n").center(width))
    var = input("\t\t\t Eleccion : " )
    return var
def Primer_Menu():
    width = 78
    while (True):
        os.system('clear')
        print(("Bienvenido a su agenda telefonica !\n").center(width))
        nom = input("\t\tIngrese su nombre : ")
        print("\n")
        print(("Muchas gracias %s ahora siga las instrucciones.\n"%nom).center(width))
        print(("1. ) Cagar sus contactos desde un csv existente. \n").center(width))
        print(("2. ) Empezar el programa. \n").center(width))
        var = input("\t\t\t Eleccion : " )
        try:
            if (int(var) == 1 ):
                path = input("\t\tIngrese el nombre/ruta del archivo : ")
                return cargar_csv(path,nom)

            else :
                return Agenda(nom)
        except ValueError :
            width = 78
            print("\n")
            print ("Ingrese SOLO valores numericos ! \n\n".center(width))
            print ("Presione una tecla para continuar.....\n\n".center(width))
            input()
def handler_eleccion (val , agenda):
    width = 78
    try :
        if val == 1: ## AGREGAR CONTACTO
            print ("\n")
            name = input("\t\tIngrese el nombre del contacto : ")
            numero = input("\t\tIngrese el numero del contacto : ")
            direccion = input("\t\tIngrese la direccion del contacto [vacio para default] : ")
            estado = input("\t\tIngrese Bien/Mal para el estado del contacto [vacio para default] : ")

            if direccion == "":
                direccion = "-"
            if estado.lower() ==  "mal":
                estado = False
            else :
                estado = True
            agenda.contactos = Persona(numero,name,direccion,estado)
        elif val == 2: ## ELIMINAR CONTACTO
            print ("\n")
            name = input("\t\tIngrese el nombre del contacto : ")
            print("\n")
            print(agenda.borrar_contacto(name))
        elif val == 3: ## LISTAR CONTACTOS
            while (True):
                ban = True
                while ban:
                    try :
                        el = int(Print_Menu_List())
                        ban = False
                    except ValueError:
                        width = 78
                        print("\n")
                        print ("Ingrese SOLO valores numericos ! \n\n".center(width))
                        print ("Presione una tecla para continuar.....\n\n".center(width))
                        input()
                print("\n")
                if el == 1 : ## TODOS LOS CONTACTOS
                    print(agenda.list_all)
                    break
                elif  el == 2 : ## LOS QUE ME CAEN BIEN
                    print(agenda.me_cae_bien)
                    break
                elif el == 3 : ## LOS QUE ME CAEN MAL
                    print(agenda.me_cae_mal)
                    break
                else : ## CASO POR DEFECTO
                    print("\n")
                    print ("Eleccion incorrecta ! \n\n".center(width))

        elif val == 4: ## BUSCAR INFO CONTACTOS
            print ("\n")
            name = input("\t\tIngrese el nombre del contacto : ")
            print("\n")
            print(agenda.get_info_of(name))
        elif val == 5: ## GUARDAR CONTACTOS EN CSV
            print ("\n")
            path = input("\t\tIngrese el nombre/ruta del archivo : ")
            guardar_csv(path , agenda)
        elif val == 6: ## CARGAR CONTACTOS EN CSV
            print ("\n")
            path = input("\t\tIngrese el nombre/ruta del archivo : ")
            agenda2 = cargar_csv(path , agenda.nombre)
            if agenda2.is_vacio == False:
                agenda = agenda2
        elif val == 7: ## SALIR
            print("\n")
            print ("Gracias por usar el programa ! \n\n".center(width))
            exit(0)
        else:
            print("\n")
            print ("Eleccion incorrecta ! \n".center(width))
            print("\n")
    except ValueError:
        width = 78
        print("\n")
        print ("Ingrese SOLO valores numericos ! \n\n".center(width))
        print ("Presione una tecla para continuar.....\n\n".center(width))
        os.system('clear')
        input()
def main():
    width = 78
    agenda = Primer_Menu() ;
    while (True):
        os.system('clear')
        try:
            el = int(Print_Menu(agenda.nombre))
            handler_eleccion(el,agenda)
            print("\n")
            print ("Presione una tecla para continuar.....\n\n".center(width))
            input()
        except ValueError:
            print("\n")
            print ("Ingrese SOLO valores numericos ! \n\n".center(width))
            print ("Presione una tecla para continuar.....\n\n".center(width))
            input()

if __name__ == "__main__":
    main()
