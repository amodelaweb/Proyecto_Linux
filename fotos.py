import sys
import time
import os
import datetime
from PIL import Image
from shutil import copyfile
def main():

    if len(sys.argv) != 5:
        print("\n")
        print ('\033[1;34mUsage : \033[1;m' , '\033[1;33m ' , sys.argv[0] ,' -s path_fuente -d path_destino ','\033[1;m')
        print("\n")
        exit(1)
    ban1 = False
    ban2 = False
    for i in range (0,len(sys.argv)): ## VALIDACION DE ARGUMENTOS
        if sys.argv[i] == "-s":
            path_fuente = sys.argv[i+1]
            ban1 = True
        if sys.argv[i] == "-d":
            path_destino = sys.argv[i+1]
            ban2 = True
    if ((ban1 != True or ban2 != True) or (os.path.isdir(path_fuente) != True or os.path.isdir(path_destino) != True) ): ##VALIDACION DE PATHS
        print("\n")
        print ('\033[1;34mERROR!! \033[1;m' , '\033[1;33m ' , sys.argv[0] ,' Paths o banderas inexistentes','\033[1;m')
        print("\n")
        exit(1)
    availed_formats = ['.jpg','.jpeg','.jpge','.png','.gif','.nef','.cr2','.crw','.tiff']
    availed_formats2 = ['.mp4','.avi']

    archivos = os.listdir(path_fuente)
    os.chdir(path_fuente)
    for x in archivos:
        ban = 0

        nom , ext = os.path.splitext(x)
        for y in availed_formats:
            if ext.lower() == y.lower() :
                ban = 1
                break
        for y in availed_formats2:
            if ext.lower() == y.lower() :
                ban = 2
                break
        if ban == 1 or ban == 2:
            if ban == 1 :
                img = Image.open(x)
                exif_data = img._getexif()
                if (exif_data == None):
                    if (time.gmtime(os.path.getctime(x)).tm_mday < 10):
                        dia = "0"+str(time.gmtime(os.path.getctime(x)).tm_mday)
                    else :
                        dia = str(time.gmtime(os.path.getctime(x)).tm_mday)
                    if (time.gmtime(os.path.getctime(x)).tm_mon < 10):
                        mes = "0"+str(time.gmtime(os.path.getctime(x)).tm_mon)
                    else :
                        mes = str(time.gmtime(os.path.getctime(x)).tm_mon)
                    name = dia+"-"+mes+ "-"+str(time.gmtime(os.path.getctime(x)).tm_year)
                else :
                    exif_data = img._getexif()[36867]
                    exif_data = exif_data.split(" ")
                    exif_data = exif_data[0].split(":")
                    name =  exif_data[2]+"-"+exif_data[1]+"-"+exif_data[0]
                path_fecha = path_destino+"/"+name
                if (os.path.exists(path_fecha) == False):
                    os.makedirs(path_fecha)
                path_thum = path_fecha + "/" +".thumbs"
                if (os.path.exists(path_thum) == False):
                    os.makedirs(path_thum)
                copyfile(x,path_destino+"/"+name+"/"+x)
                image = Image.open(x)
                image.thumbnail((100, 100), Image.ANTIALIAS)
                image.save(path_thum+"/"+"."+x, 'JPEG')
            else :
                if (time.gmtime(os.path.getctime(x)).tm_mday < 10):
                    dia = "0"+str(time.gmtime(os.path.getctime(x)).tm_mday)
                else :
                    dia = str(time.gmtime(os.path.getctime(x)).tm_mday)
                if (time.gmtime(os.path.getctime(x)).tm_mon < 10):
                    mes = "0"+str(time.gmtime(os.path.getctime(x)).tm_mon)
                else :
                    mes = str(time.gmtime(os.path.getctime(x)).tm_mon)
                name = dia+"-"+mes+ "-"+str(time.gmtime(os.path.getctime(x)).tm_year)
                path_fecha = path_destino+"/"+name
                if (os.path.exists(path_fecha) == False):
                    os.makedirs(path_fecha)
                copyfile(x,path_destino+"/"+name+"/"+x)
    print("\n")
    print ('\033[1;34mRESULTADO : \033[1;m' , '\033[1;33m Se ha realizado el proceso correctamente. \033[1;m')
    print("\n")

if __name__ == "__main__":
    main()
